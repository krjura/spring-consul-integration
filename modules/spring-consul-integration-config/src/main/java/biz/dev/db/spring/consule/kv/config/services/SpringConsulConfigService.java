package biz.dev.db.spring.consule.kv.config.services;

import java.util.Properties;

public interface SpringConsulConfigService {

    Properties loadAggregatedProperties(String... profiles);
}
