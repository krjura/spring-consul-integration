package biz.dev.db.spring.consule.kv.config.services;

import biz.dev.db.spring.consule.core.ConsulException;
import biz.dev.db.spring.consule.core.services.ConsulApiService;
import biz.dev.db.spring.consule.core.services.impl.ConsulApiServiceImpl;
import biz.dev.db.spring.consule.kv.services.ConsulApiKvService;
import biz.dev.db.spring.consule.kv.services.ConsulApiKvServiceImpl;
import biz.dev.db.spring.consule.kv.services.pojo.KvEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class SpringConsulConfigServiceImpl implements SpringConsulConfigService {

    private static final Logger logger = LoggerFactory.getLogger(SpringConsulConfigServiceImpl.class);

    private ConsulApiService coreService;

    private ConsulApiKvService kvService;

    private static final String PARAM_CONSUL_APPLICATION_BASE_URL = "consul.application.base.url";
    private static final String PARAM_CONSUL_APPLICATION_BLOB_URL = "consul.application.blob.url";
    private static final String PARAM_CONSUL_FORMAT = "consul.format";

    private String paramApplicationBase;
    private String paramBlobUrl;
    private String paramFormat;

    public SpringConsulConfigServiceImpl(
            ConsulApiService coreService,
            ConsulApiKvService kvService) {

        this.coreService = coreService;
        this.kvService = kvService;

        init();
    }

    private void init() {
        this.paramApplicationBase = this.coreService.getProperties().getProperty(PARAM_CONSUL_APPLICATION_BASE_URL);
        this.paramBlobUrl = this.coreService.getProperties().getProperty(PARAM_CONSUL_APPLICATION_BLOB_URL);
        this.paramFormat = this.coreService.getProperties().getProperty(PARAM_CONSUL_FORMAT);
    }

    private boolean checkIsBlobFormat() {
        return this.paramFormat.equals("PROPERTIES");
    }

    @Override
    public Properties loadAggregatedProperties(String... profiles) {
        if(this.paramFormat.equals("PROPERTIES")) {
            return loadAggregatedPropertiesFormat(profiles);
        } else {
            throw new ConsulException("unsupported format: " + this.paramFormat);
        }
    }

    private Properties loadAggregatedPropertiesFormat(String... profiles) {
        if(logger.isDebugEnabled()) {
            logger.debug("received the following profiles {} to aggregate", Arrays.toString(profiles));
        }

        List<KvEntry> entries = loadProfiles(profiles);

        Properties properties = new Properties();

        for( KvEntry entry : entries ) {

            if( entry.getValue() == null) {
                if( logger.isDebugEnabled()) {
                    logger.debug("KvEntry value is null for {}", entry);
                }

                continue;
            }

            if(logger.isDebugEnabled()) {
                logger.debug("Loading the following entry: {}", entry);
            }

            loadPropertiesFromString(properties, entry.getValue());
        }

        return properties;
    }

    private List<KvEntry> loadProfiles(String... profiles) {
        if(logger.isDebugEnabled()) {
            logger.debug("received the following profiles: {}", Arrays.toString(profiles));
        }

        String[] nodes = new String[profiles.length];

        for( int i = 0; i < profiles.length; i++) {
            String nodeName = profiles[i];
            String expandedNodeName = getNodeUrl(nodeName);

            if( logger.isDebugEnabled()) {
                logger.debug("nodeName {} was expanded to ", nodeName, expandedNodeName);
            }

            nodes[i] = expandedNodeName;
        }

        return this.kvService.loadNodesRecursively(nodes);
    }

    private String getNodeUrl(String profile) {
        boolean isBlobFormat = checkIsBlobFormat();

        if( isBlobFormat ) {
            return this.paramApplicationBase + "/" + profile + "/" + this.paramBlobUrl;
        } else {
            return this.paramApplicationBase + "/" + profile;
        }
    }

    private void loadPropertiesFromString(Properties properties, String text) {
        try {
            if( logger.isDebugEnabled()) {
                logger.debug("Loading the following text as properties '{}'", text);
            }

            properties.load(new StringReader(text));
        } catch (IOException e) {
            throw new ConsulException("Cannot load properties from text: " + text, e);
        }
    }

    public static void main(String[] args) throws MalformedURLException {
        ConsulApiServiceImpl coreService = new ConsulApiServiceImpl();

        ConsulApiKvServiceImpl kvService = new ConsulApiKvServiceImpl(coreService);

        SpringConsulConfigService configService = new SpringConsulConfigServiceImpl(coreService, kvService);

        Properties properties = configService.loadAggregatedProperties("default", "local", "production");
        System.out.println(properties);
    }
}
