package biz.dev.db.spring.consule.core.services.impl;

import biz.dev.db.spring.consule.core.ConsulException;
import biz.dev.db.spring.consule.core.services.ConsulPropertiesService;
import biz.dev.db.spring.consule.core.utils.UriUtils;
import org.junit.Test;

import java.util.Properties;

import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.equalTo;

public class ConsulPropertiesServiceImplTest {

    @Test(expected = ConsulException.class)
    public void testDefaultConfigurationDoesNotExist() {
        ConsulPropertiesService service = new ConsulPropertiesServiceImpl(
                UriUtils.toUri("classpath:/test-unknown-consul.properties"),
                UriUtils.toUri("classpath:/test-unknown-consul.properties")
        );

        service.getProperties();
    }

    @Test
    public void testDefaultProperties() {
        ConsulPropertiesService service = new ConsulPropertiesServiceImpl(
                UriUtils.toUri("classpath:/test-unknown-consul.properties"),
                UriUtils.toUri("classpath:/test-default-consul.properties")
        );

        Properties properties = service.getProperties();

        assertThat(properties.getProperty("consul.base.url"), is(equalTo("http://example.com")));
        assertThat(properties.getProperty("consul.application.base.url"), is(equalTo("example/config")));
        assertThat(properties.getProperty("consul.format"), is(equalTo("PROPERTIES")));
        assertThat(properties.getProperty("consul.application.data.url"), is(equalTo("exampleData")));
    }

    @Test
    public void testPropertiesOverride() {
        ConsulPropertiesService service = new ConsulPropertiesServiceImpl(
                UriUtils.toUri("classpath:/test-known-consul.properties"),
                UriUtils.toUri("classpath:/test-default-consul.properties")
        );

        Properties properties = service.getProperties();

        assertThat(properties.getProperty("consul.base.url"), is(equalTo("http://example.org")));
        assertThat(properties.getProperty("consul.application.base.url"), is(equalTo("example/config")));
        assertThat(properties.getProperty("consul.format"), is(equalTo("YAML")));
        assertThat(properties.getProperty("consul.application.data.url"), is(equalTo("exampleData")));
    }

}
