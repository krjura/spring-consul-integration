package biz.dev.db.spring.consule.core.services;

import java.util.Properties;

public interface ConsulPropertiesService {

    Properties getProperties();
}
