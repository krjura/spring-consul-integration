package biz.dev.db.spring.consule.core.services.impl;

import biz.dev.db.spring.consule.core.ConsulException;
import biz.dev.db.spring.consule.core.services.ConsulApiService;
import biz.dev.db.spring.consule.core.services.ConsulPropertiesService;
import biz.dev.db.spring.consule.core.utils.UriUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.Map;
import java.util.Properties;

public class ConsulApiServiceImpl implements ConsulApiService {

    private static final Logger logger = LoggerFactory.getLogger(ConsulApiServiceImpl.class);

    private static final String PARAM_CONSUL_BASE_URL = "consul.base.url";

    private RestTemplate restTemplate;

    private ConsulPropertiesService propertiesService;

    public ConsulApiServiceImpl() {
        this(
                new RestTemplate(),
                new ConsulPropertiesServiceImpl(
                        UriUtils.toUri("/consul-bootstrap.properties"),
                        UriUtils.toUri("/default-consul-bootstrap.properties")
                )
        );
    }

    public ConsulApiServiceImpl(RestTemplate restTemplate, ConsulPropertiesService consulPropertiesService) {
        this.restTemplate = restTemplate;
        this.propertiesService = consulPropertiesService;
    }

    public Properties getProperties() {
        return this.propertiesService.getProperties();
    }

    @SuppressWarnings("unchecked")
    public List<Map<String, Object>> getAsList(String url, Map<String, Object> urlParams)  {

        String completeUrl = buildCompleteUrl(url, urlParams);

        if(logger.isDebugEnabled()) {
            logger.debug("sending request to the following url: {}", completeUrl);
        }

        try {
            ResponseEntity<List> responseEntity = this
                    .restTemplate
                    .getForEntity(completeUrl, List.class, urlParams);

            List<Map<String, Object>> body = responseEntity.getBody();

            if( logger.isDebugEnabled()) {
                logger.debug("received the following response: '{}' for url: '{}'", body, completeUrl);
            }

            return body;
        } catch (RestClientException e) {
            throw new ConsulException("Failed to fetch data from consul on url: " + completeUrl, e);
        }

    }

    private String buildCompleteUrl(String url, Map<String, Object> urlParams) {

        String baseUrl = getBaseURl();

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl + url);

        for( Map.Entry<String, Object> entries : urlParams.entrySet() ) {
            builder.queryParam(entries.getKey(), entries.getValue());
        }

        return builder.build().encode().toString();
    }

    private String getBaseURl() {
        return (String) this.propertiesService.getProperties().get(PARAM_CONSUL_BASE_URL);
    }

    public static void main(String[] args) {
        ConsulApiServiceImpl service = new ConsulApiServiceImpl();
        service.getProperties();
    }
}
