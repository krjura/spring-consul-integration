package biz.dev.db.spring.consule.core.services.impl;

import biz.dev.db.spring.consule.core.ConsulException;
import biz.dev.db.spring.consule.core.services.ConsulPropertiesService;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class ConsulPropertiesServiceImpl implements ConsulPropertiesService {

    private static final String SCHEME_TYPE_FILE = "file";
    private static final String SCHEME_TYPE_CLASSPATH = "classpath";
    private static final List<String> SUPPORTED_SCHEMES = new ArrayList<>(
            Arrays.asList(SCHEME_TYPE_FILE, SCHEME_TYPE_CLASSPATH));

    private URI applicationProperties;
    private URI defaultProperties;

    private Properties properties;

    public ConsulPropertiesServiceImpl(URI applicationProperties, URI defaultProperties) {
        verifyUri(applicationProperties);
        verifyUri(defaultProperties);

        this.applicationProperties = applicationProperties;
        this.defaultProperties = defaultProperties;
    }

    private void verifyUri(URI uri) {
        if(SUPPORTED_SCHEMES.contains(uri.getScheme())) {
            return;
        }

        throw new ConsulException("Unsupported scheme for uri: " + uri.toString());
    }

    public Properties getProperties() {
        if(this.properties == null) {
            init();
        }

        return properties;
    }

    private InputStream getStream(URI uri) {
        try {
            switch (uri.getScheme()) {
                case SCHEME_TYPE_FILE:
                    return new FileInputStream(uri.getPath());
                case SCHEME_TYPE_CLASSPATH:
                    return ConsulPropertiesServiceImpl.class.getResourceAsStream(uri.getPath());
                default:
                    throw new ConsulException("Unsupported scheme for uri: " + uri.toString());
            }
        } catch (FileNotFoundException e) {
            throw new ConsulException("file not found for uri: " + uri.toString());
        }
    }

    private void init() {

        InputStream defaultStream = getStream(this.defaultProperties);

        if(defaultStream == null ) {
            throw new ConsulException("Cannot find default consul bootstrap properties");
        }

        InputStream applicationStream = getStream(this.applicationProperties);

        Properties properties = new Properties();
        try {
            properties.load(defaultStream);
            defaultStream.close();

            if( applicationStream != null) {
                properties.load(applicationStream);
                applicationStream.close();
            }

            this.properties = properties;

        } catch (IOException e) {
            throw new ConsulException("Cannot load properties from classpath file", e);
        }
    }

    public static void main(String[] args) throws URISyntaxException {
        ConsulPropertiesService service = new ConsulPropertiesServiceImpl(
                new URI("classpath:/consul-bootstrap.properties"),
                new URI("classpath:/default-consul-bootstrap.properties")
        );

        Properties properties = service.getProperties();
        System.out.println(properties);
    }
}
