package biz.dev.db.spring.consule.core.services;

import java.util.List;
import java.util.Map;
import java.util.Properties;

public interface ConsulApiService {

    Properties getProperties();

    List<Map<String, Object>> getAsList(String url, Map<String, Object> urlParams);
}
