package biz.dev.db.spring.consule.core;

public class ConsulException extends RuntimeException {

    public ConsulException(String message) {
        super(message);
    }

    public ConsulException(String message, Throwable cause) {
        super(message, cause);
    }
}
