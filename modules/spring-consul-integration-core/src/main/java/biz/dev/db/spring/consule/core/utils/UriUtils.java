package biz.dev.db.spring.consule.core.utils;

import biz.dev.db.spring.consule.core.ConsulException;

import java.net.URI;
import java.net.URISyntaxException;

public class UriUtils {

    public static URI toUri(String uri) {
        try {
            return new URI(uri);
        } catch (URISyntaxException e) {
            throw new ConsulException("Cannot parse uri: " + uri, e);
        }
    }
}
