package biz.dev.db.spring.consule.kv.services;

import biz.dev.db.spring.consule.core.services.ConsulApiService;
import biz.dev.db.spring.consule.core.services.impl.ConsulApiServiceImpl;
import biz.dev.db.spring.consule.kv.services.pojo.KvEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConsulApiKvServiceImpl implements ConsulApiKvService {

    private static final Logger logger = LoggerFactory.getLogger(ConsulApiKvServiceImpl.class);

    private static final String PARAM_LOCK_INDEX = "LockIndex";
    private static final String PARAM_KEY = "Key";
    private static final String PARAM_FLAGS = "Flags";
    private static final String PARAM_VALUE = "Value";
    private static final String PARAM_CREATE_INDEX = "CreateIndex";
    private static final String PARAM_MODIFY_INDEX = "ModifyIndex";

    private static final String PARAM_CONSUL_URL_KV = "consul.url.kv";

    private ConsulApiService coreService;

    private String paramKvApi;

    public ConsulApiKvServiceImpl(ConsulApiService coreService) {
        this.coreService = coreService;

        init();
    }

    public ConsulApiKvServiceImpl() {
        this(new ConsulApiServiceImpl());
    }

    private void init() {
        this.paramKvApi = (String) this.coreService.getProperties().get(PARAM_CONSUL_URL_KV);
    }

    @Override
    public List<KvEntry> loadNodes(String... nodes) {
        if(logger.isDebugEnabled()) {
            logger.debug("loading nodes: {}", Arrays.toString(nodes));
        }

        List<KvEntry> entries = new ArrayList<>();

        for( String node : nodes ) {
            entries.addAll(getKvEntries(node));
        }

        return entries;
    }

    @Override
    public List<KvEntry> loadNodesRecursively(String... nodes) {
        if(logger.isDebugEnabled()) {
            logger.debug("loading nodes recursively: {}", Arrays.toString(nodes));
        }

        List<KvEntry> entries = new ArrayList<>();

        for( String node : nodes ) {
            entries.addAll(getKvEntries(node, true));
        }

        return entries;
    }

    @Override
    public List<KvEntry> getKvEntries(String node) {
        return getKvEntries(node, true);
    }

    @Override
    public List<KvEntry> getKvEntries(String node, boolean recurse) {
        HashMap<String, Object> urlParams = getUrlParams(recurse);

        String contextUrl = this.paramKvApi + node;

        if(logger.isDebugEnabled()) {
            logger.debug("constructed the following consul context url: {}", contextUrl);
        }

        List<Map<String, Object>> mapEntries = coreService.getAsList(contextUrl, urlParams);

        if(logger.isDebugEnabled()) {
            logger.debug("received the following data '{}' from url '{}' ", mapEntries, contextUrl);
        }

        List<KvEntry> kvEntries = new ArrayList<>();

        for( Map<String, Object> mapEntry : mapEntries ) {

            KvEntry entry = new KvEntry();

            for( Map.Entry<String, Object> param : mapEntry.entrySet() ) {

                switch (param.getKey()) {
                    case PARAM_LOCK_INDEX:
                        entry.setLockIndex((Integer) param.getValue());
                        break;
                    case PARAM_KEY:
                        entry.setKey((String) param.getValue());
                        break;
                    case PARAM_FLAGS:
                        entry.setFlags((Integer) param.getValue());
                        break;
                    case PARAM_VALUE:
                        entry.setValue(base64Decode((String) param.getValue()));
                        break;
                    case PARAM_CREATE_INDEX:
                        entry.setCreateIndex((Integer) param.getValue());
                        break;
                    case PARAM_MODIFY_INDEX:
                        entry.setModifyIndex((Integer) param.getValue());
                        break;
                    default:
                        // do nothing
                        break;
                }
            }

            kvEntries.add(entry);
        }

        if(logger.isDebugEnabled()) {
            logger.debug("received the following entries {}", kvEntries);
        }

        return kvEntries;
    }

    private HashMap<String, Object> getUrlParams(boolean recurse) {
        HashMap<String, Object> urlParams = new HashMap<>();

        if( recurse) {
            urlParams.put("recurse", "true");
        }

        return urlParams;
    }

    private String base64Decode(String raw) {
        if( raw == null) {
            return null;
        }

        return new String(Base64.getDecoder().decode(raw));
    }
}
