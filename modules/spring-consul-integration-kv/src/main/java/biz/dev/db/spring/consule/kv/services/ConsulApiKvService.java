package biz.dev.db.spring.consule.kv.services;

import biz.dev.db.spring.consule.kv.services.pojo.KvEntry;

import java.util.List;

public interface ConsulApiKvService {

    List<KvEntry> loadNodesRecursively(String... nodes);

    List<KvEntry> loadNodes(String... nodes);

    List<KvEntry> getKvEntries(String node);

    List<KvEntry> getKvEntries(String node, boolean recurse);
}
