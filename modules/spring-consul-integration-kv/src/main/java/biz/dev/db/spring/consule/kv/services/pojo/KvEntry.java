package biz.dev.db.spring.consule.kv.services.pojo;

public class KvEntry {

    private Integer lockIndex;

    private String key;

    private Integer flags;

    private String value;

    private Integer createIndex;

    private Integer modifyIndex;

    public Integer getLockIndex() {
        return lockIndex;
    }

    public void setLockIndex(Integer lockIndex) {
        this.lockIndex = lockIndex;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Integer getFlags() {
        return flags;
    }

    public void setFlags(Integer flags) {
        this.flags = flags;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getCreateIndex() {
        return createIndex;
    }

    public void setCreateIndex(Integer createIndex) {
        this.createIndex = createIndex;
    }

    public Integer getModifyIndex() {
        return modifyIndex;
    }

    public void setModifyIndex(Integer modifyIndex) {
        this.modifyIndex = modifyIndex;
    }

    @Override
    public String toString() {
        return "KvEntry{" +
                "lockIndex=" + lockIndex +
                ", key='" + key + '\'' +
                ", flags=" + flags +
                ", value='" + value + '\'' +
                ", createIndex=" + createIndex +
                ", modifyIndex=" + modifyIndex +
                '}';
    }
}
