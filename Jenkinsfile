pipeline {
  agent {
    docker {
      image 'krjura/build_env_02'
      label 'docker'
      args '-v /opt/jenkins/gradle/cache:/opt/gradle/cache'
    }
  }

  triggers {
    pollSCM('@daily')
  }

  environment {
    SONAR_HOST=credentials('sonarqubeHost')
    SONAR_USERNAME=credentials('sonarqubeUsername')
    SONAR_PASSWORD=credentials('sonarqubePassword')
    ARTIFACTORY_USERNAME=credentials('artifactoryUsername')
    ARTIFACTORY_PASSWORD=credentials('artifactoryPassword')
    GRADLE_DEFAULTS=' --no-daemon --info -g /opt/gradle/cache '
  }

  options {
    buildDiscarder(logRotator(numToKeepStr: '10'))
    disableConcurrentBuilds()
    timeout(time: 1, unit: 'HOURS')
    timestamps()
  }
  
  stages {
    stage ('Initialize') {
      steps {
        sh '''
          echo "PATH = ${PATH}"
        '''
      }
    }
    stage("Build and Test") {
      steps {
        checkout scm
        sh 'gradle $GRADLE_DEFAULTS clean build'
        junit '**/test-results/test/TEST-*.xml'
      }
    }
    stage("Analyse") {
      when {
        expression {
          return env.BRANCH_NAME == 'master' || env.BRANCH_NAME == 'development' 
        }
      }
      steps {
        sh 'gradle $GRADLE_DEFAULTS test sonarqube -Dsonar.host.url=$SONAR_HOST -Dsonar.login=$SONAR_USERNAME -Dsonar.password=$SONAR_PASSWORD -Dsonar.branch=$BRANCH_NAME'
      }
    }
    stage("Deploy") {
      when {
        expression {
          return env.BRANCH_NAME == 'master' || env.BRANCH_NAME == 'development'
        }
      }

      steps {
	sh 'echo "deploy"'
	sh 'gradle $GRADLE_DEFAULTS artifactoryPublish -PartifactoryUser=$ARTIFACTORY_USERNAME -PartifactoryPassword=$ARTIFACTORY_PASSWORD -PbuildName=jenkins -PbuildNumber=$BUILD_NUMBER'
      }
    }
  }
}
